import cv2
import numpy as np
import matplotlib.pyplot as plt


def main():
    # Récupération de l'image
    nom_fichier = "image.png"
    img = cv2.imread(nom_fichier, cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # TRANSLATION
    # Taille de l'image
    rows, columns, channels = img.shape

    # Matrice de transformation
    # Translation de a pixels vers la droite
    # Translation de b pixels vers le haut
    T = np.float32([[1, 0, a], [0, 1, -b]])
    translation = cv2.warpAffine(img, T, (columns, rows))

    titres = ['Original', 'Translation']
    images = [img, translation]

    for i in range(len(titres)):
        plt.subplot(1, len(titres), i + 1)
        plt.imshow(images[i])
        plt.title(titres[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


if __name__ == "__main__":
    main()
