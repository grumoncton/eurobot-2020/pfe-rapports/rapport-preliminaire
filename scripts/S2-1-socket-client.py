import socket


IP_ADDRESS = '192.168.0.80'
PORT = 8080


# Ouvrir un "socket"
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:

    # Connecter au serveur (robot primaire)
    sock.connect((IP_ADDRESS, PORT))

    # Envoyer un message
    sock.send(b'Message du client')

    # Recevoir une réponse
    print(sock.recv(4096))
