import numpy as np
import cv2
import cv2.aruco as aruco


def traiter_photo(img_arr):

    # Convertit la photo en niveaux de gris
    gray = cv2.cvtColor(img_arr, cv2.COLOR_BGR2GRAY)

    # Récupération du dictionnaire
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_100)

    # Paramètre de détection
    aruco_params = aruco.DetectorParameters_create()

    # Trouve les marqueurs ArUco
    corners, ids, rejected = aruco.detectMarkers(
        image=gray, dictionary=aruco_dict, parameters=aruco_params
    )

    # Vérifie si `ids` a des valeurs
    if isinstance(ids, np.ndarray) and ids.any():
        # Dessine un carré autour du marqueur et donne le numéro
        aruco.drawDetectedMarkers(img_arr, corners, ids)

    return img_arr


def main():
    # Récupération de la photo
    img = cv2.imread("QualiteRequiseBCAPG.png")

    # Récupération de la photo traitée
    res = traiter_photo(img)

    # Enregistrement du résultat
    cv2.imwrite("NeedsMore.jpg", res)


if __name__ == "__main__":
    main()
