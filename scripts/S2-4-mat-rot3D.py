import numpy as np


def calcul_matrice_rotation(theta):
    """
    Calcule une matrice de rotation à partir d'un tuple
    contenant des angles d'Euler
    theta = (theta_x, theta_y, theta_z)
    """
    R_x = np.array(
        [
            [1, 0, 0],
            [0, np.cos(theta[0]), -np.sin(theta[0])],
            [0, np.sin(theta[0]), np.cos(theta[0])],
        ]
    )
    R_y = np.array(
        [
            [np.cos(theta[1]), 0, np.sin(theta[1])],
            [0, 1, 0],
            [-np.sin(theta[1]), 0, np.cos(theta[1])],
        ]
    )
    R_z = np.array(
        [
            [np.cos(theta[2]), -np.sin(theta[2]), 0],
            [np.sin(theta[2]), np.cos(theta[2]), 0],
            [0, 0, 1],
        ]
    )
    R = np.linalg.multi_dot([R_x, R_y, R_z])
    return R


def main():
    angles = tuple(np.radians(ang) for ang in (120, 90, 180))
    R = calcul_matrice_rotation(angles)


if __name__ == "__main__":
    main()
