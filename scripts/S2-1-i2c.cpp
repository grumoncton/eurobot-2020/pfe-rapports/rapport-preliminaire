#include <Arduino.h>
#include <i2c_driver.h>
#include "imx_rt1060/imx_rt1060_i2c_driver.h"

const uint16_t slave_address = 0x002D;
I2CSlave& slave = Slave;
void after_receive(int size);

const size_t slave_rx_buffer_size = 32;
uint8_t slave_rx_buffer_volatile[slave_rx_buffer_size] = {};
uint8_t slave_rx_buffer[slave_rx_buffer_size] = {};
volatile size_t slave_bytes_received = 0;

const size_t slave_tx_buffer_size = 32;
uint8_t slave_tx_buffer[slave_tx_buffer_size] = {};


volatile bool before_transmit_received = false;
volatile bool after_transmit_received = false;
volatile bool buffer_underflow_detected = false;
void log_message_received();
void log_transmit_events();
void after_transmit();
void before_transmit_isr();

int x = 0;

void setup() {

  // Configurer I2C
  slave.after_receive(after_receive);
  slave.set_receive_buffer(slave_rx_buffer_volatile, slave_rx_buffer_size);

  slave.before_transmit(before_transmit_isr);
  slave.after_transmit(after_transmit);
  slave.set_transmit_buffer(slave_tx_buffer, slave_tx_buffer_size);

  slave.listen(slave_address);

  Serial.begin(115200);
}

void loop() {
	if (slave_bytes_received) {
		log_message_received();

		slave_bytes_received = 0;

		x++;
		memcpy(slave_tx_buffer, &x, sizeof(x));

		log_transmit_events();
	}

	delay(1);
}

void after_receive(int size) {
	// Copier les donnes du tampon
	// Seul temps où on est sur qu'il ne change pas
	if (!slave_bytes_received) {
		memcpy(slave_rx_buffer, slave_rx_buffer_volatile, size);
		slave_bytes_received = size;
	}
}

void log_message_received() {
	Serial.print(slave_bytes_received);
	Serial.print(" bytes: ");
	for(size_t i = 0; i < slave_bytes_received; i++) {
		Serial.print(slave_rx_buffer[i]);
		Serial.print(", ");
	}
	Serial.println();
}

void before_transmit_isr() {
	before_transmit_received = true;
}

void after_transmit() {
	after_transmit_received = true;
}

void log_transmit_events() {
	if (after_transmit_received) {
		after_transmit_received = false;
	}
}
