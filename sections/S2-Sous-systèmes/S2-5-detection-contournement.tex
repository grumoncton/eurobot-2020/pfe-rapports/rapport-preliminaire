% vim: spelllang=fr


\subsection{Détection / Contournement} \label{S2-5-detection-contournement}

Puisque le contact entre les robots est interdit, véhicules autonomes sont obligés d'inclure un système de détection des adversaires. Le système de détection doit conformer aux règlements de la compétition. La liste de contraintes du tableau~\ref{table:S2-5-contraintes-regles} fut établie en fonction de ces règlements (annexe~\ref{appendix:regles}).

\begin{table}[H]
	\caption{Contraintes provenant des règles de la compétition relatives au système de détection des robots adverses (annexe~\ref{appendix:regles})}
	\label{table:S2-5-contraintes-regles}
	\centering
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		\textbf{Section} & \textbf{Contrainte} \\
		\midrule
		F.4. & Les équipes sont tenues d’équiper leur(s) robot(s) d’un système de détection des robots adverses. \\
		F.5.b. & Les lasers de classe: \\
					 & \tabitem 1 et 1M sont acceptés sans restriction \\
					 & \tabitem 2 sont tolères si le rayon laser n'est jamais projeté en dehors de l'aire de jeu \\
					 & \tabitem 2M, 3R, 3B et 4 sont formellement interdits \\
		G.6. & Au-delà des bordures de l'aire de jeu, il peut y avoir des éléments pouvant perturber la détection des couleurs ou des signaux de communications. En aucun cas il n'est possible de demander aux personnes de s'écarter ou de bouger des éléments de décors autour de faire de jeu. \\
		\bottomrule
	\end{tabularx}
\end{table}

En somme, les robots doivent être en mesure de détecter les robots adverses dans leur chemin et d'arrêter avant d'entrer en collision avec l'adversaire. Puisqu'il est interdit de bouger les éléments au-delà de l'aire de jeu, les robots doivent être en mesure de filtrer ce qui est véritablement un adversaire.

Si un système à base de laser est utilisé pour la détection, il doit inclure uniquement des lasers de classe 1 ou 1M.

Si le robot arrête face à un adversaire, il conforme déjà aux contraintes de la compétition. Cependant, il serait bloqué et, si le robot adverse ne se déplace pas, il ne gagnera plus de points supplémentaires. Afin d'éviter cette situation, les robots devraient implémenter un système de contournement capable de générer un nouveau chemin sans obstacles. Le robot pourrait alors suivre ce nouveau chemin et se débloquer.

\subsubsection{Choix de la méthode de détection}

\paragraph{Cahier des charges}

La performance du système de détection est très importante; il doit être en mesure de répondre aux contraintes du tableau~\ref{table:S2-5-contraintes-regles}. Afin de permettre au système d'entrainement de réagir rapidement à l'apparition d'un adversaire, il doit s actualiser fréquemment. Sa portée doit être suffisante afin de détecter des robots lointains. Plus loin les adversaires lors de leur détection, plus facile le contournement.  La position des obstacles détecté doit être précise en termes de distance ainsi qu'en termes d'angle. Finalement, l'espace couvert par les capteurs devrait être maximisé. En autres mots, les angles morts devraient être minimes.

Plusieurs alternatives existent pour le système de détection. Certaines sont plus complexes que d'autres. Vu la durée du projet et la quantité de tâches à faire, les alternatives simples seront favorisées. La simplicité est difficile a quantifier. Pourtant, elle est incluse vu son importance.

Finalement, puisque le budget du projet est limité, le système doit être abordable. Cependant, puisque le système de détection est important non seulement pour conformer aux règles de la compétition, mais aussi pour contourner des obstacles et pour se débloquer en proximité des adversaires, le prix est moins important que la performance.

Tous ces critères furent résumés dans le cahier des charges du tableau~\ref{table:S2-5-cahier-des-charges}.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de détection}
	\label{table:S2-5-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{500}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{700}{\$}: \SI{75}{\percent}$ \\
			$> \SI{700}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Performance} & $\SI{50}{\percent}$ \\

		\midrule

		Fréquence d'actualisation & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{10}{\hertz}: \SI{100}{\percent}$ \\
			$\geq \SI{5}{\hertz}: \SI{75}{\percent}$ \\
			$\geq \SI{1}{\hertz}: \SI{50}{\percent}$ \\
			$< \SI{1}{\hertz}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Portée & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{3.6}{\meter}: \SI{100}{\percent}$ \\
			$\geq \SI{1}{\meter}: \SI{80}{\percent}$ \\
			$\geq \SI{200}{\centi\meter}: \SI{60}{\percent}$ \\
			$< \SI{200}{\centi\meter}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Précision sur la distance & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{1}{\milli\meter}: \SI{100}{\percent}$ \\
			$\leq \SI{5}{\milli\meter}: \SI{60}{\percent}$ \\
			$> \SI{5}{\milli\meter}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Précision angulaire & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{1}{\degree}: \SI{100}{\percent}$ \\
			$\leq \SI{10}{\degree}: \SI{75}{\percent}$ \\
			$\leq \SI{15}{\degree}: \SI{50}{\percent}$ \\
			$> \SI{15}{\degree}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Espace couvert & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{95}{\percent}: \SI{100}{\percent}$ \\
			$\geq \SI{75}{\percent}: \SI{75}{\percent}$ \\
			$\geq \SI{50}{\percent}: \SI{50}{\percent}$ \\
			$< \SI{50}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Simplicité} & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{60}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Alternatives considérées}

Afin de maximiser la réutilisation des composantes, les seules alternatives considérées sont celles pour lesquelles le GRUM possède déjà les composantes. Les alternatives sont le capteur LiDAR, les capteurs ultrason et les capteurs temps de vol.

\subparagraph{LiDAR}

Les capteurs LiDAR projettent un faisceau laser et utilisent le temps de vol avant la détection de la réflexion afin de déterminer la distance de l'obstacle. Ce type de détection fut utilisé par l'équipe l'année passée. En effet, le LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec} fut le capteur de détection des deux robots pour la compétition \textit{Eurobot 2019}. Un aperçu du capteur est donné à la figure~\ref{fig:S2-5-rplidar}. Un survol de ses caractéristiques est donné au tableau~\ref{fig:S2-5-rplidar-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-rplidar.jpg}
	\caption{Capteur LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec}~\cite{RPLIDAR-home}}
	\label{fig:S2-5-rplidar}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques du capteur LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec}~\cite{RPLIDAR-datasheet}}
	\label{fig:S2-5-rplidar-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Classe du laser & Classe 1 \\
		Portée & $\SI{12}{\meter}$ \\
		Intervalle angulaire & $\SI{360}{\degree}$ \\
		Planéité de l'espace détecté & $\pm\SI{1.5}{\degree}$ \\
		Précision sur la distance & \makecell[l]{
			$< \SI{0.5}{\milli\meter}$ (pour des distances $< \SI{1.5}{\meter}$) \\
			$< \SI{1}{\percent}$ (pour toutes autres distances) \\
		} \\
		Précision angulaire typique & $\SI{0.9}{\degree}$ \\
		Fréquence d'actualisation maximale & $\SI{15}{\hertz}$ \\
		Prix & $\SI{425}{\$}$~\cite{RPLIDAR-robotshop} \\
		\bottomrule
	\end{tabularx}
\end{table}

\subparagraph{Capteurs ultrason}

Les capteurs ultrason projettent une impulsion sonore à basse fréquence (ultrason) et utilisent le temps de vol avant la détection de la réflexion afin de déterminer la distance de l'obstacle. Le GRUM a utilisé de tels capteurs pour la détection pour les compétions \textit{Eurobot 2017} et \textit{Eurobot 2018}. Contrairement au LiDAR, le capteur ultrason ne tourne pas sur lui même. Il faudrait utiliser plusieurs capteurs afin d'approximer les $\SI{360}{\degree}$ de détection du LiDAR.

La figure~\ref{fig:S2-5-ultras-bob} montre l'ensemble de capteurs ultrason constituant le système de détection du robot secondaire de la compétition de 2018. Ce système emploie huit capteurs afin d'approximer l'espace $\SI{360}{\degree}$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-ultras-bob.jpg}
	\caption{Ensemble de huit capteurs ultrason utilisé pour la détection en 2018}
	\label{fig:S2-5-ultras-bob}
\end{figure}

Les capteurs utilisés dans ce système sont le HC-SR04 de \textit{Elec Freaks}. Un aperçu de ce capteur est présenté à la figure~\ref{fig:S2-5-ultra}. Un survol de ses caractéristiques est donné au tableau~\ref{fig:S2-5-ultra-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-ultra.jpg}
	\caption{Capteur HC-SR04 de \textit{Elec Freaks}~\cite{ultra-sparkfun}}
	\label{fig:S2-5-ultra}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques du capteur ultrason HC-SR04 \textit{Elec Freaks}~\cite{ultra-datasheet}}
	\label{fig:S2-5-ultra-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{80}{\centi\meter}$ \footnotemark \\
		Intervalle angulaire & $\pm\SI{15}{\degree}$ \\
		Précision sur la distance & $\SI{3}{\milli\meter}$ \\
		Fréquence d'actualisation maximale & $\SI{16.67}{\hertz}$ \\
		Prix & $\SI{3.95}{\$}$~\cite{ultra-sparkfun} \\
		\bottomrule
	\end{tabularx}
\end{table}

\footnotetext{La fiche de données donne une portée de $\SI{4}{\meter}$~\cite{ultra-datasheet}. Pourtant, P. Marian donne une portée effective de $\SI{80}{\centi\meter}$~\cite{ultra}. En effet, il est seulement possible de détecter des obstacles à la portée maximale dans les conditions idéaux. L'expérience du GRUM correspond à la portée effective de P. Marian en pratique.}

Avec les données du tableau~\ref{fig:S2-5-ultra-caractéristiques}, il est possible de déterminer les caractéristiques du système à huit capteurs présenté à la figure~\ref{fig:S2-5-ultras-bob}. Vu l'intervalle angulaire du capteur, la précision angulaire du système sera également $\pm\SI{15}{\degree}$. Puisque huit capteurs sont utilisés et chacun détecté dans un zone de $\SI{30}{\degree}$, l'espace couvert par les capteurs sera $\SI{66.67}{\percent}$, tel que montre l'équation~\ref{eq:S2-5-ultra-espace}. Ce calcul est optimiste. Les capteurs détectent principalement au centre. Le tableau~\ref{fig:S2-5-systemeultra-caractéristiques} montre les propriétés du système entier.

\begin{align}
	\eta &= \frac{\SI{30}{\degree} \cdot 8}{\SI{360}{\degree}} = \SI{66.67}{\percent}
	\label{eq:S2-5-ultra-espace}
\end{align}

\begin{table}[H]
	\caption{Caractéristiques du système de détection utilisant huit capteurs ultrason HC-SR04}
	\label{fig:S2-5-systemeultra-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{80}{\centi\meter}$ \\
		Planéité de l'espace détecté & $\pm\SI{15}{\degree}$ \\
		Précision sur la distance & $\SI{3}{\milli\meter}$ \\
		Précision angulaire & $\pm\SI{15}{\degree}$ \\
		Espace couvert & $\SI{66.67}{\percent}$ \\
		Fréquence d'actualisation maximale & $\SI{16.67}{\hertz}$ \\
		Prix & $\SI{31.60}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\subparagraph{Capteurs temps de vol}

Les capteurs temps de vol sont très similaires aux capteurs ultrason. Contraient aux capteurs ultrason qui envoient des impulsions sonores, les capteurs temps de vol envoient des impulsions lumineux. A la compétition \textit{Eurobot 2019}, le GRUM a utilisé de capteurs temps de vol VL53L0X de \textit{Pololu} (basé sur le circuit intégré VL53L0X de \textit{STMicroelectronics}). Ces mêmes capteurs furent choisis comme une des solutions finales pour le positionnement à la section~\ref{S2-2-choix-tof}. Ils furent utilisés non pour la détection, mais pour le positionnement. Un aperçu du capteur est donné à la figure~\ref{fig:S2-5-tof}. De plus, une liste de ses caractéristiques pertinentes pour la détection est présentée au tableau~\ref{table:S2-5-tof-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-VL53L0X.jpg}
	\caption{Capteur temps de vol VL53L0X de \textit{Pololu}~\cite{VL53L0X}}
	\label{fig:S2-5-tof}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques pertinentes du capteur temps de vol VL53L0X en mode d'opération \textquote{standard}~\cite{VL53L0X}~\cite{VL53L0X-datasheet}}
	\label{table:S2-5-tof-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Classe du laser & Classe 1 \\
		Portée & $\SI{1.2}{\meter}$ \\
		Intervalle angulaire & $\pm\SI{12.5}{\degree}$ \\
		Précision sur la distance & $\SI{1}{\milli\meter}$ \\
		Fréquence d'actualisation maximale & $\SI{33.33}{\hertz}$ \\
		Prix & $\SI{9.95}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Il serait possible d'utiliser ces capteurs dans une configuration similaire à celle des capteurs ultrason de la figure~\ref{fig:S2-5-ultras-bob}. Le système résultant aura les caractéristiques du tableau~\ref{fig:S2-5-systeme-tof-caractéristiques}.

\begin{table}[H]
	\caption{Caractéristiques du système de détection utilisant huit capteurs temps de vol VL53L0X}
	\label{fig:S2-5-systeme-tof-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{1.2}{\meter}$ \\
		Planéité de l'espace détecté & $\pm\SI{12.5}{\degree}$ \\
		Précision sur la distance & $\SI{1}{\milli\meter}$ \\
		Précision angulaire & $\pm\SI{12.5}{\degree}$ \\
		Espace couvert & $\SI{55.56}{\percent}$ \\
		Fréquence d'actualisation maximale & $\SI{33.33}{\hertz}$ \footnotemark \\
		Prix & $\SI{79.60}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\footnotetext{Cette valeur est basée sur le temps de lecture d'un capteur. Techniquement, il serait possible de lire chacun simultanément. Cependant, en pratique, tous les capteurs seraient probablement sur le même bus \ItwoC, ce qui réduirait la fréquence d'actualisation.}

Il est possible de voir que le système à base de capteurs temps de vol est plus précis et a une meilleur portée que le système à base de capteurs ultrason. Le compromis de cette meilleur précision angulaire est plus d'angles morts. De plus, le système est le double du prix du système ultrasonique.

\paragraph{Étude de faisabilité}

Avant de faire la sélection de la méthode de détection avec une matrice décisionnelle, une étude de faisabilité fut effectuée sur chacune des alternatives. Les options furent évaluées sur les axes de faisabilité physique, effectivité, simplicité et faisabilité économique. Les résultats de cette étude sont présentés au tableau~\ref{table:S2-5-detection-faisabilité}.

Le capteur LiDAR fut constaté faisable sur tous les quatre axes. En effet, ce capteur a été très effectif pour le GRUM l'année dernière.

Le système à bases d'HC-SR04 fut déterminé satisfaisant avec conditions sur l'axe d'effectivité. En effet, les angles morts que présente le système sont problématique. De plus, l'alternative a été jugée complexe avec conditions. Le système exige des supports pour les capteurs. De plus, il faudra probablement un microcontrôleur pour lire les distances des capteurs et pour les transmettre à l'ordinateur central. Pourtant, l'alternative est acceptée.

Le système exploitant les capteurs VL53L0X, tout comme celui utilisant les HC-SR04, fut déterminé satisfaisant avec conditions sur l'axe d'effectivité. En effet, les angles morts de ce systèmes sont encore plus sévère. De plus, le système a été constaté simple avec conditions. Puisque les huit capteurs sont sur le même bus \ItwoC, la lecture des valeurs est légèrement plus simple. Pourtant, il faudrait encore un microcontrôleur pour transmettre les valeurs au microcontrôleur central. Toutefois, l'alternative est acceptée.

\begin{table}[H]
	\caption{Étude de la faisabilité des trois alternatives pour le système de détection}
	\label{table:S2-5-detection-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Effectivité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		LiDAR & Oui & Oui & Oui & Oui & Acceptée \\
		Capteurs ultrason & Oui & Oui, mais & Non, mais & Oui & Acceptée \\
		Temps de vol & Oui & Oui, mais & Oui, mais & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Décision finale de la méthode de détection}

La matrice de décision du tableau~\ref{table:S2-5-detection-matrice} fut utilisée afin de choisir la méthode de détection en fonction du cahier des charges du tableau~\ref{table:S2-5-cahier-des-charges} et des trois alternatives considérées. Le RPLIDAR A2M8 fut choisi comme système de détection, vu son score parfait.

\begin{table}[H]
	\caption{Matrice de décision utilisé pour choisir le système de détection}
	\label{table:S2-5-detection-matrice}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{LiDAR} & \textbf{Ultrason} & \textbf{Temps de vol} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		\rowgroup{Performance} & $\SI{50}{\percent}$ \\

		\midrule

		Fréquence d'actualisation & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		Portée & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{0}{\percent}$ & $\SI{80}{\percent}$ \\

		\midrule

		Précision sur la distance & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{60}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		Précision angulaire & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{50}{\percent}$ & $\SI{50}{\percent}$ \\

		\midrule

		Espace couvert & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{50}{\percent}$ & $\SI{50}{\percent}$ \\

		\midrule

		\rowgroup{Simplicité} & $\SI{30}{\percent}$ & $\SI{100}{\percent}$ & $\SI{60}{\percent}$ & $\SI{60}{\percent}$ \\

		\toprule

		\rowgroup{Total} & - & $\SI{100}{\percent}$ & $\SI{64}{\percent}$ & $\SI{76}{\percent}$ \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Choix de la stratégie de contournement}

\paragraph{Cahier des charges}

Pour ne pas être bloqué face aux adversaires, le système de contournement doit être en mesure de trouver un chemin qui permet d'atteindre la destination sans entrer en collision avec les obstacles. Puisque le système de positionnement et le système de détection ne sont pas parfaits, des facteurs de sécurité doivent être ajoutés.

Les alternatives seront évaluées selon leur temps de traitement, si elles génèrent des chemin globalement optimaux et leur simplicité d'implémentation. Le temps de traitement est difficile a quantifier, mais est une critère importante vu la durée des matchs et la complexité potentielle de l'algorithme utilisé. La simplicité est aussi difficile a quantifier. Pourtant, encore une fois, elle est incluse vu l'échéance du projet et la quantité de tâches à faire. Ces critères furent résumes dans le cahier des charges du tableau~\ref{table:S2-5-contournement-cahier-des-charges}.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de contournement}
	\label{table:S2-5-contournement-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		Temps de traitement & $\SI{30}{\percent}$ & \makecell{
			$\leq \SI{0.1}{\second}: \SI{100}{\percent}$ \\
			$\leq \SI{1}{\second}: \SI{75}{\percent}$ \\
			$\leq \SI{2}{\second}: \SI{50}{\percent}$ \\
			$> \SI{2}{\second}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Génération des chemins optimaux & $\SI{40}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Simplicité & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{60}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Alternatives considérées}

Les deux méthodes de contournement considérées sont une approche discrète et une approche continue.

\subparagraph{Méthode discrète}

La méthode discrète de contournement consiste \textit{discrétiser} le terrain de jeu. C'est-à-dire, de diviser l'espace continue en grille de points discrètes. Le terrain discrétisé pourrait alors être représenté en tant que graphe (sens informatique) et les méthodes de recherche de chemin discrètes (tel que le classique algorithme de Dijkstra~\cite{Dijkstra1959}) peuvent y être appliqué.

Cette approche est celle qui fut implémentée par le GRUM pour les dernières trois compétitions. En effet, l'aire de jeu fut divisé en grille de points et l'algorithme de Dijkstra fut appliqué. Un exemple de ce recherche de chemin sur le terrain de jeu d'\textit{Eurobot 2019} est présenté à la figure~\ref{fig:S2-5-chemin-dijkstra}. Dans cet exemple, le chemin (série de points) est déterminé entre l'origine à ($\SI{100}{\milli\meter}$, $\SI{100}{\milli\meter}$) et la destination à ($\SI{1750}{\milli\meter}$, $\SI{2750}{\milli\meter}$). Les points de la grille sont marqués en bleu, les points faisant partie du chemin en rouge, et les obstacles en noir.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-chemin-dijkstra.png}
	\caption{Chemin entre le point ($\SI{100}{\milli\meter}$, $\SI{100}{\milli\meter}$) et le point ($\SI{1750}{\milli\meter}$, $\SI{2750}{\milli\meter}$) sur une grille discrète calculé avec l'algorithme de Dijkstra}
	\label{fig:S2-5-chemin-dijkstra}
\end{figure}

Avec cette approche, il faut faire un compromis entre le temps d'exécution et la qualité du chemin. En effet, afin d'optimiser le temps, pas tous les points sont connectés. En conséquence, le chemin résultant n'est pas optimal. Un autre moyen d'augmenter la qualité du chemin est de diminuer l'espace entre les points. Pourtant, cela augmente dramatiquement le temps d'exécution.

Vu ces désavantages de cette approche, la recherche de chemin n'a jamais été utilisé fortement par le GRUM, et jamais pour contourner un robot adverse.

\subparagraph{Méthode continue}

La méthode continue de la recherche de chemin tente à trouver un trajet entre l'origine et la destination sans discrète l'aire de jeu. L'algorithme pourrait itérer un chemin jusqu'au moment où il ne contient plus aucuns obstacles. Des formules géométriques seraient utilisées afin de calculer les distances et les obstacles bloquants un chemin. Ces calculs seront légèrement plus complexes que ceux de l'algorithme Dijkstra. Cependant, il faut significativement moins de calculs. Le nombre de calculs sera au plus proportionnel au nombre d'obstacles et non au nombre de points. De plus, l'algorithme devrait trouver le chemin globalement optimal.

\paragraph{Décision finale de la stratégie de contournement} \label{S2-5-choix-contournement}

Puisque la méthode discrète n'est pas capable de calculer des chemins globalement optimal, il est constaté qu'elle ne répond pas au cahier des charges du tableau~\ref{table:S2-5-contournement-cahier-des-charges}. Ainsi, la méthode continue est choisie comme méthode de contournement.

\subsubsection{Tâches à faire}

Avec les capteurs et la méthode de contournement déterminés, les tâches restantes consistent majoritairement de la programmation. Ces tâches sont détaillées dans les sous-sections suivantes.

\paragraph{Filtrage des faux-positifs}

Le système de détection verra des obstacles dans les $\SI{360}{\degree}$ autour de lui. Puisque ces obstacles pourraient être des robots adverses, des éléments de jeu ou des obstacles au-delà de l'aire de jeu, le système doit être en mesure de filtrer les obstacles détectes et de revernir seulement ceux qui sont véritablement dans le chemin.

Pour faire ce tri, le système aura besoin de convertir les obstacles détectés des coordonnées relatives au robot en coordonnées absolues par rapport au terrain de jeu. Cela exige évidemment un système de positionnement qui permet de calculer très précisément l'angle du robot. Sinon, tout petit erreur résultera dans un grand erreur de la position de l'obstacle par rapport à la table. Par la suite, il est possible de déterminer si l'obstacle est un élément de jeu en fonction du terrain préprogrammé du robot.

\paragraph{Implémentation de divers formes de la zone de détection}

Auparavant, la zone d'arrêt (la zone où la présence d'un obstacle produira un arrêt immédiat du robot) prenait la forme d'un quart de cercle devant le robot. La forme de la zone est présenté à la figure~\ref{table:S2-5-zone-detection-ancien}. Cette forme est loin d'être optimal. En effet, elle met trop d'importance sur les obstacles loin sur les cotés et pas assez sur les obstacles proches sur les cotés. Le problème est illustré par l'exemple de la figure~\ref{table:S2-5-zone-detection-ancien-probleme}. Le robot rouge est beaucoup plus proche et présente beaucoup plus de danger de collision que le robot vert. Pourtant, contrairement au robot rouge, le robot vert se trouve dans la zone d'arrêt et sa présence produira un freinage du robot.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-ancien.png}
	\caption{Zone d'arrêt utilisé pour la compétition \textit{Eurobot 2019}}
	\label{table:S2-5-zone-detection-ancien}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-ancien-probleme.png}
	\caption{Problème avec la zone d'arrêt utilisé pour la compétition \textit{Eurobot 2019}}
	\label{table:S2-5-zone-detection-ancien-probleme}
\end{figure}

Lors des remue méninges de détection, il fut proposé d'avoir une zone qui arrête le robot et une zone plus large qui ralentis le robot. La zone d'arrêt pourrait prendre la forme d'un rectangle. Puisque le robot se déplace lentement, le rectangle pourrait être seulement légèrement plus large que lui. Un aperçu de la forme d'arrêt proposée est présentée à la figure~\ref{table:S2-5-zone-detection-arreter}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-arreter.png}
	\caption{Zone d'arrêt rectangulaire proposée}
	\label{table:S2-5-zone-detection-arreter}
\end{figure}

La zone de ralentissement pourrait prendre la forme d'un ellipse. Elle serait plus large et plus long que la zone rectangulaire afin de ralentir le robot si un adversaire entre du coté. Un aperçu de la zone elliptique proposée est présenté à la figure~\ref{table:S2-5-zone-detection-ralentir}. L'exemple de la figure~\ref{table:S2-5-zone-detection-exemple} montre que le même adversaire rouge fait en sorte que le robot ralentis, mais puisqu'il n'est pas directement dans le chemin, le robot n'arrête pas.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-5-zone-detection-rallentir.png}
		\caption{Zone elliptique}
		\vspace{12pt}
	\end{subfigure}\hfill
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-5-zone-detection-superposition.png}
		\caption{Superposition de la zone elliptique et de la zone rectangulaire}
	\end{subfigure}
	\caption{Zone de ralentissement elliptique proposée}
	\label{table:S2-5-zone-detection-ralentir}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-exemple.png}
	\caption{Zone d'arrêt rectangulaire proposée}
	\label{table:S2-5-zone-detection-exemple}
\end{figure}

Il faudrait expérimenter avec la largeur et la rongeüre de ces zones afin de trouver des formes qui réussissent à arrêter le robot avant une collusion se produise, mais qui ne sont pas trop sensible de telle façon que le robot arrête dans des situations qui ne sont pas dangereuses. Tout cela dépend évidement des dimensions du robot et de sa vitesse de déplacement.

\paragraph{Implémentation de l'algorithme de recherche de chemin}

Cette tâche s'agit de la programmation de l'algorithme continue de recherche de chemin décidé à la section~\ref{S2-5-choix-contournement}. L'algorithme doit prendre comme entrée une coordonnée origine et une coordonnée destination et doit trouver le chemin le plus court en fonction des obstacles. Le temps de traitement devrait être minime. Idéalement, le traitement devrait prendre une fraction de seconde.

\subsubsection{Échéances}

Les tâches présentées aux sous-sections précédentes furent insérées dans le diagramme de Gantt de la figure~\ref{table:S2-5-gantt}. Il est possible de voir que toutes les tâches de programmation sont aoutement en cours et ne sont pas encore commencées. En effet, vu le délai des commandes de circuits imprimés, la conception de ces derniers fut priorités. Maintenant que les circuits sont conçu, les tâches de programmations peuvent être accentuées.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-echeance.png}
	\caption{Diagramme de Gantt représentant l'échéance des tâches pour les systèmes de détection et de contournement}
	\label{table:S2-5-gantt}
\end{figure}

\subsubsection{Travail accompli}

\paragraph{Interface graphique montrant les obstacles}

Une interface graphique fut développée qui montre les obstacles détectés par le LiDAR. Un aperçu de l'interface est présenté à la figure~\ref{table:S2-5-detection-pointmap}. L'interface a été développer afin de plus facilement écrire du code pour le LiDAR. En effet, voir ce que le capteur détecter simplifie énormément la tâche. Ce programme pourrait être élaboré afin de montrer la zone d'arrêt et la zone de ralentissement.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-detection-pointmap.png}
	\caption{Interface graphique montrant les obstacles détectés par le capteur LiDAR}
	\label{table:S2-5-detection-pointmap}
\end{figure}

\paragraph{Algorithme de contournement}

Prime d'abord, les algorithmes de recherche de chemin dans des espaces continues furent recherchés. Plusieurs articles ont été étudiés, notamment \textquote{Robot Path Planning with Avoiding Obstacles in Known Environment Using Free Segments and Turning Points Algorithm}~\cite{Hassani2018}. L'article propose un algorithme qui tente de déterminer le chemin le plus court et le plus sécuritaire dans le contexte d'un robot mobile à entrainement différentiel. L'algorithme est intéressant et semble correspondre aux besoins. Pourtant, les chemin générés par cet algorithme ne sont pas optimaux.

Il fut ainsi décidé de développer un algorithme de zéro en utilisant l'article de Hassani, Maalej et Rekik comme inspiration. L'algorithme proposé a deux étapes: il faut d'abord trouver une liste de chemins qui ne passent pas par un obstacle, puis il faut trouver le chemin le plus court parmi la liste.

Le diagramme de flux de la figure~\ref{fig:S2-5-contournement-algorithme-chemins-debloques} montre l'algorithme proposé pour trouver les chemins non bloqués. Initialement, la liste de chemin potentiels contient un seul chemin: une ligne droite de l'origine à la destination. Si le chemin ne passe pas par un obstacle, la recherche est finie. Sinon, le chemin qui passe par dessus celui qui passe en dessous l'obstacle sont considérés (ajoutés à la liste de chemins potentiels). L'algorithme exécute le test encore une fois sur les nouveaux chemins et itère de cette façon jusqu'au moment où la liste contient que des chemins débloqués.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[node distance=2cm]
		\node (start) [startstop] {Début};

		\node (initial) [process, below of=start, yshift=-0.5cm, text width=5cm] {Chemin initial: ligne droite entre origine et destination (séquence de deux points)};
		\draw [arrow] (start) -- (initial);

		\node (blocked) [decision, below of=initial, yshift=-3cm, text width=5cm] {Est-ce que le chemin passe par un obstacle?};
		\draw [arrow] (initial) -- (blocked);

		\node (dupe) [process, below of=blocked, yshift=-2.5cm] {Dupliquer le chemin};
		\draw [arrow] (blocked) -- node [anchor=east] {Oui} (dupe);

		\node (above) [process, below of=dupe, xshift=-3cm, text width=5cm] {Ajouter un point au chemin pour aller au dessus de l'obstacle};
		\draw [arrow] (dupe) -- (above);
		\draw [arrow] (above) -- ++(-3.5,0) -- ++(0,1) -- (blocked);

		\node (below) [process, below of=dupe, xshift=3cm, text width=5cm] {Ajouter un point au chemin pour aller en dessous l'obstacle};
		\draw [arrow] (dupe) -- (below);
		\draw [arrow] (below) -- ++(3.5,0) -- ++(0,1) -- (blocked);

		\node (end) [startstop, right of=blocked, xshift=4cm] {Fin};
		\draw [arrow] (blocked) -- node [anchor=south] {Non} (end);

	\end{tikzpicture}

	\caption{Diagramme de flux de l'algorithme proposé pour la recherche des chemins non bloqués}
	\label{fig:S2-5-contournement-algorithme-chemins-debloques}
\end{figure}

Avec une liste de chemins non bloqués calculée, il suffit de suivre le plus court. Cela est déterminé assez facilement avec de la somme des hypoténuses entre les points.

Le diagramme de flux de l'algorithme a été développé. Pourtant, l'algorithme n'a pas encore été implémenté en \textit{Python}.
