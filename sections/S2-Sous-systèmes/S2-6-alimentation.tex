% vim: spelllang=fr

\subsection{Alimentation} \label{S2-6-alimentation}

Étant des systèmes électriques, les robots requièrent des alimentations électriques. Le système d'alimentation doit être autosuffisant et doit conformer aux règlements de la compétition. La liste de contraintes du tableau~\ref{table:S2-6-contraintes-regles} fut établie en fonction de ces règlements (annexe~\ref{appendix:regles}).

\begin{table}[H]
	\caption{Contraintes provenant des règles de la compétition relatives au système d'alimentation (annexe~\ref{appendix:regles})}
	\label{table:S2-6-contraintes-regles}
	\centering
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		\textbf{Section} & \textbf{Contrainte} \\
		\midrule
		F.3. & Seules des batteries non modifiées peuvent être utilisées. \\
		F.3. & Les équipes doivent être en mesure de jouer trois matchs de suite. \\
		F.3. & Batteries en permanences dans des sacs ignifuges certifiés et non modifiés. \\
		F.4. & Bouton d'arrêt d'urgence de robots autonomes. L'appui sur ce bouton doit provoquer l'arrêt immédiat de tous les actionneurs du robot. \\
		F.5.a & Les tensions embarquées ne doivent  pas dépasser $\SI{48}{\volt}$. \\
		\bottomrule
	\end{tabularx}
\end{table}

En somme, les robots devraient être alimentés par des batteries de tension inférieure à $\SI{48}{\volt}$ non modifiées dans des sacs ignifuges. Le système d'alimentation doit être en mesure d'alimenter le robot pour trois matchs consécutifs. Le bouton d'arrêt d'urgence doit être en mesure d'arrêter tous actionneurs du robot.

\subsubsection{Cahier des charges}

Avant de concevoir un circuit d'alimentation, il faut d'abord déterminer les exigences énergétiques des robots. Le tableau~\ref{table:S2-6-liste-composantes-batterie} liste les composantes qui seront alimentées directement de la batterie. Étant donné la large gamme de tensions d'alimentation du \textit{ODrive}, il sera probablement possible de l'alimenter directement, selon le choix de batterie.

\begin{table}[H]
	\caption{Composantes électriques alimentées directement par la batterie}
	\label{table:S2-6-liste-composantes-batterie}
	\centering
	\begin{tabularx}{\textwidth}{Xlll}
		\toprule
		\textbf{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\
		\midrule
		\textit{ODrive} & $\num{20}$ \cite{odrive-docs} & 1 & $\num{20}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Le tableau~\ref{table:S2-6-liste-composantes-12v} présente les composantes électriques qui seront alimentées à $\SI{12}{\volt}$. Ces composantes doivent alors être alimentées via un régulateur de courant (hacheur C.C.). Afin d'utiliser le même circuit d'alimentation pour les deux robots, il faut que la sortie $\SI{12}{\volt}$ puisse fournir au moins le courant de la section commune plus le courant maximal entre les sections exclusives à un tel robot. Avec un facteur de sécurité de quinze pour cent, la capacité minimale du régulateur $\SI{12}{\volt}$ est alors $\SI{4.054}{\ampere}$.

\begin{table}[H]
	\caption{Composantes électriques alimentées à $\SI{12}{\volt}$}
	\label{table:S2-6-liste-composantes-12v}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}llll}
		\toprule
		\rowgroup{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\
		\midrule

		\rowgroup{Commun entre les deux robots} \\

		Raspberry Pi + Carte Fille & $\num{1.325}$ (section~\ref{S2-2-travail-commun}) & 1 & $\num{1.325}$ \\

		\midrule
		\rowgroup{Exclusif au robot primaire} \\

		Moteur C.C. avec balais & $\num{1.1}$ \cite{moteur} & 2 & $\num{2.2}$ \\

		\midrule
		\rowgroup{Exclusif au robot secondaire} \\

		Pompes à vide & $\num{0.25}$ \cite{pompe} & 8 & $\num{2}$ \\

		\midrule
		\hspace{-1em}Total & - & - & $\num{3.525}$ \\
		\rowgroup{Total + facteur de sécurité ($\SI{15}{\percent}$)} & - & - & $\num{4.054}$ \\

		\bottomrule
	\end{tabularx}
\end{table}

Le tableau~\ref{table:S2-6-liste-composantes-6v} présente les composantes alimentées à $\SI{6}{\volt}$. Encore une fois, un régulateur de tension doit être utilisé afin de fournir cette tension constante. Puisque le robot secondaire exige plus de courant à cette tension, il dicte la capacité minimale du régulateur. Avec un facteur de sécurité de quinze pour cent, le régulateur $\SI{6}{\volt}$ doit alors être en mesure de fournir au moins $\SI{10.35}{\ampere}$.

\begin{table}[H]
	\caption{Composantes électriques alimentées à $\SI{6}{\volt}$}
	\label{table:S2-6-liste-composantes-6v}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xlll}
		\toprule
		\rowgroup{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\

		\midrule
		\rowgroup{Exclusif au robot primaire} \\

		Servomoteur \textquote{micro} & $\num{1}$ \cite{micro-servo} & 2 & $\num{2}$ \\

		\midrule
		\rowgroup{Exclusif au robot secondaire} \\

		Servomoteur \textquote{micro} & $\num{1}$ \cite{micro-servo} & 4 & $\num{4}$ \\
		Servomoteur & $\num{1}$ & 5 & $\num{5}$ \\

		\midrule
		\hspace{-1em}Total & - & - & $\num{9}$ \\
		\rowgroup{Total + facteur de sécurité ($\SI{15}{\percent}$)} & - & - & $\num{10.35}$ \\

		\bottomrule
	\end{tabularx}
\end{table}

Avec ces courants total calculés, il fut possible d'établir le cahier des charges pour le système d'alimentation du tableau~\ref{table:S2-6-cahier-des-charges}. Vu que la fonction principale du système d'alimentation est de fournir les sources d'énergies à des tensions précises, une emphase est mise sur la section \textquote{régulation de tension}. Cette section spécifie que les sorties $\SI{12}{\volt}$ et $\SI{6}{\volt}$ devraient être précises et efficaces et doivent fournir les courants requis par les systèmes du robot.

Une autre grande section est la protection et la vérification. La carte doit se protéger contre une surtension, un surcourant et une polarisation inverse. De plus, elle devrait vérifier la santé de la batterie. C'est-à-dire, elle assure que les cellules sont chargées de façon balancée. Le circuit devrait aussi afficher la tension de la batterie afin de montrer quand elle doit être échangée. Des indicateurs tels que des DELs seront incorporés dans le circuit afin de montrer l'état du système. Il serait possible d'utiliser une DEL pour l'état de l'arrêt d'urgence, une autre pour l'état de la batterie et une dernière pour l'état des régulateurs.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système d'alimentation}
	\label{table:S2-6-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Prix} & $\SI{15}{\percent}$ & \makecell{
			$\leq \SI{250}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{300}{\$}: \SI{50}{\percent}$ \\
			$> \SI{300}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Régulation de tension} & $\SI{50}{\percent}$ \\
		\midrule

		Erreur sur la sortie & $\SI{12.5}{\percent}$ & \makecell{
			$\leq \SI{1}{\percent}: \SI{100}{\percent}$ \\
			$\leq \SI{5}{\percent}: \SI{75}{\percent}$ \\
			$> \SI{5}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Rendement du hachage & $\SI{12.5}{\percent}$ & \makecell{
			$\geq \SI{90}{\percent}: \SI{100}{\percent}$ \\
			$\geq \SI{75}{\percent}: \SI{75}{\percent}$ \\
			$< \SI{75}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Courant de la sortie $\SI{12}{\volt}$ & $\SI{12.5}{\percent}$ & - & $\SI{4.054}{\ampere}$ & - \\

		\midrule

		Courant de la sortie $\SI{6}{\volt}$ & $\SI{12.5}{\percent}$ & - & $\SI{10.35}{\ampere}$ & - \\

		\midrule
		\rowgroup{Protection / Vérification} & $\SI{20}{\percent}$ \\
		\midrule

		Protection surtension & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Protection surcourant & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Protection polarité inverse & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Vérification de la batterie & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Vérification des régulateurs & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule
		\rowgroup{Rétroaction} & $\SI{10}{\percent}$ \\
		\midrule

		\makecell[l]{
			Affichage de la tension \\
			de la batterie \\
		} & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Indicateurs de l'état du système & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Choix des composantes du système d'alimentation}

Avec le cahier des charges établie, les sous-sections suivantes justifieront les choix des composantes électriques utilisées dans le système d'alimentation.

\paragraph{Choix de la batterie}

Étant donnée la gamme de tension d'alimentation du \textit{ODrive} de $\SI{12}{\volt}$ à $\SI{24}{\volt}$ \cite{odrive-docs}, la tension de la batterie devrait tomber dans cette gamme. De plus, vu le régulateur de $\SI{12}{\volt}$, la tension de la batterie devrait être au moins quelques volts supérieur à cette tension afin de combattre la chute de tension du hacheur série. En utilisant une chute typique d'environ $\SI{2}{\volt}$, la tension de la batterie devrait être entre $\SI{14}{\volt}$ et $\SI{24}{\volt}$. Ainsi, la contrainte de $\SI{48}{\volt}$ du tableau~\ref{table:S2-6-contraintes-regles} n'est pas un problème.

En plus de ces contraintes sur la tension, la batterie devrait avoir une capacité suffisante pour alimenter le robot pour un match plein. Idéalement, elle alimenterait le robot pour jusqu'à trois matchs consécutifs sans exiger un changement de batterie.

La batterie utilisée dans les robots l'année dernière, le \textit{TURNIGY} 2650 mAh 4S, réponds au cahier des charges. Afin de réutiliser au maximum les composantes, aucunes alternatives furent considérées et cette batterie fut choisie comme source d'alimentation pour les robots. Un aperçu de la batterie est donné à la figure~\ref{fig:S2-6-lipo} et ses caractéristiques sont données au tableau~\ref{table:S2-6-lipo-caractéristiques}. La batterie a une capacité de $\SI{2.65}{\ampere\hour}$ et, puisque la configuration est de quatre cellules de $\SI{3.7}{\volt}$ chacun en série, a une tension nominale de $\SI{14.8}{\volt}$. Elle répond parfaitement au cahier des charges du tableau~\ref{table:S2-6-cahier-des-charges}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{./figures/S2-sous-systèmes/S2-6-lipo.jpg}
	\caption{Batterie LiPo \textit{TURNIGY} 2650 mAh 4S \cite{lipo}}
	\label{fig:S2-6-lipo}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques de la \textit{TURNIGY} 2650 mAh 4S \cite{lipo}}
	\label{table:S2-6-lipo-caractéristiques}

	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Capacité & $\SI{2650}{\milli\ampere\hour}$ \\
		Configuration & 4S1P \\
		Tension nominale & $\SI{14.8}{\volt}$ \\
		Décharge constante & 20C \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Choix des régulateurs de tension}

La batterie étant choisie, il est possible de calculer le courant d'entrée des régulateurs grâce à la formule de l'équation~\ref{eq:S2-6-calcul-courant-entrée}, où $I_{entr\acute{e}e}$ représente le courant entrant dans le régulateur, $I_{sortie}$ représente le courant sortant du régulateur, $V_{r\acute{e}g}$ la tension du régulateur et $V_{nom}$ la tension nominale de la batterie.

\begin{align}
	I_{entr\acute{e}e} &= \frac{V_{r\acute{e}g}}{V_{nom}} \cdot I_{sortie}
	\label{eq:S2-6-calcul-courant-entrée}
\end{align}

L'équation~\ref{eq:S2-6-calcul-courant-entrée} est utilisé afin de calculer les courants d'entrés des régulateurs de $\SI{12}{\volt}$ et de $\SI{6}{\volt}$ en fonction de leurs courants de sorties. Ces valeurs sont données au tableau~\ref{table:S2-6-courants-sortant}. Il est à noter que ce calcul suppose que le régulateur a un rendement unitaire. Cependant, ces valeurs donnent l'ordre des courants requis et permettront de choisir un régulateur. Une fois cette composante déterminé, il serait possible de calculer des courant réalistes en utilisant le rendement donné.

\begin{table}[H]
	\caption{Courants entrants en fonction des courants sortants des régulateurs de tension}
	\label{table:S2-6-courants-sortant}
	\begin{tabularx}{\textwidth}{lXX}
		\toprule
		\textbf{Tension ($\si{\volt}$)} & \textbf{Courant à tension sortie ($\si{\ampere}$)} & \textbf{Courant à tension d'entrée ($\si{\ampere}$)} \\
		\midrule
		12 & $\num{4.054}$ & $\num{3.287}$ \\
		6 & $\num{10.35}$ & $\num{4.196}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

L'année passée, trois régulateurs furent utilisés: le \textit{D24V50F5}, le \textit{D24V150F6} et le \textit{D24V150F12}. Tous les trois sont des produits de \textit{Pololu}. Les propriétés de ces régulateurs sont donnés au tableau~\ref{table:S2-6-regulateurs-Pololu}.

\begin{table}[H]
	\caption{Caractéristiques des régulateurs de tension \textit{Pololu} \cite{Pololu-d24v50f5} \cite{Pololu-d24v150f6} \cite{Pololu-d24v150f12}}
	\label{table:S2-6-regulateurs-Pololu}
	\begin{tabularx}{\textwidth}{Xlll}
		\toprule
		\textbf{Régulateur} & \textbf{Tension de sortie} & \textbf{Courant de sortie} & \textbf{Rendement typique} \\
		\midrule
		\textit{D24V50F5} & $\SI{5}{\volt} \pm \SI{4}{\percent}$ & $\SI{5}{\ampere}$ & $\SI{85}{\percent}$ à $\SI{95}{\percent}$ \\
		\textit{D24V150F6} & $\SI{6}{\volt} \pm \SI{4}{\percent}$ & $\SI{15}{\ampere}$ & $\SI{80}{\percent}$ à $\SI{95}{\percent}$ \\
		\textit{D24V150F12} & $\SI{12}{\volt} \pm \SI{4}{\percent}$ & $\SI{15}{\ampere}$ & $\SI{80}{\percent}$ à $\SI{95}{\percent}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Cette année, il fut décidé de ne pas utiliser le régulateur de $\SI{5}{\volt}$. Au contraire, les microcontrôleurs $\SI{5}{\volt}$ seront alimentés via des régulateurs (sur la carte fille de la Pi, par exemple) via la source $\SI{12}{\volt}$. Cette approche permet de simplifier le système et de positionner les composantes protectrices (fusibles, diodes) avant la régulation de $\SI{5}{\volt}$, ce qui enlève la chute de tension associée à la sortie. Ce fut un problème l'an dernier: le fait d'avoir des éléments protecteurs entre le régulateur et la Pi alimentait ce dernier sous sa tension nominale.

Puisque les régulateurs \textit{D24V150F6} et \textit{D24V150F12} répondent au cahier des charges du tableau~\ref{table:S2-6-cahier-des-charges}, ces deux furent sélectionnés respectivement pour le régulateur $\SI{6}{\volt}$ et $\SI{12}{\volt}$ sans la nécessité de considérer des alternatives. Ce choix maximisera la réutilisation des composantes. Un aperçu du régulateur \textit{D24V150F12} est présenté à la figure~\ref{fig:S2-6-D24V150F12}. Le régulateur \textit{D24V150F6} n'est pas montré parce que sa forme est presque identique.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-6-D24V150F12.jpg}
	\caption{Régulateur de tension \textit{Pololu D24V150F12}~\cite{Pololu-d24v150f12}}
	\label{fig:S2-6-D24V150F12}
\end{figure}

Avec les régulateurs déterminés, il est possible de refaire les calculs du tableau~\ref{table:S2-6-courants-sortant} avec les rendements réels. Les résultats de ces calculs sont présentés au tableau~\ref{table:S2-6-courant-max}. Ces valeurs sont calculées en utilisant le pire cas pour le rendement: $\SI{80}{\percent}$. En ajoutant le courant que l'\textit{ODrive} prend directement de la batterie, il est possible de calculer un courant maximal de $\SI{29.35}{\ampere}$.

\begin{table}[H]
	\caption{Courant maximal que la batterie doit être en mesure de fournir}
	\label{table:S2-6-courant-max}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Tension} & \textbf{Courant requis de la batterie} \\
		\midrule
		12 & $\SI{4.109}{\ampere}$ \\
		6 & $\SI{5.245}{\ampere}$ \\
		Batterie & $\SI{20}{\ampere}$ \\
		\midrule
		\textbf{Total} & $\SI{29.35}{\ampere}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Choix de la stratégie d'alimentation} \label{S2-6-choix-stratégie}

Avec les composantes critiques (la batterie et les régulateurs) choisies, il fut possible d'établir une stratégie pour le circuit d'alimentation. Vu les fonctionnalités voulues pour le système (tableau~\ref{table:S2-6-cahier-des-charges}), il fut décidé d'implémenter le système d'alimentation en tant que circuit imprimé personnalisé. Les régulateurs seront inclus comme cartes filles à ce circuit.

Vu le grand nombre de fonctions voulues, il fut décidé lors d'un remue méninge de concevoir une carte numérique (c'est-à-dire, basée sur un microcontrôleur) plutôt qu'analogique.

La carte sera responsable de la protection surtension, surcourant et polarité inverse de la batterie. Elle sera également responsable de raccorder les régulateurs à la batterie ainsi que les sorties aux régulateurs appropriés. Le microcontrôleur sera en mesure d'interrompre les sorties dans le cas où la tension de la batterie est trop basse ou qu'un des régulateurs ne fonctionne pas correctement.

Le système gèrera aussi l'arrêt d'urgence, puisque la méthode le plus simple et le plus sure d'arrêter tous actionneurs est de couper leur alimentation. L'arrêt d'urgence sera l'entrée d'un autre étage de régulateurs, capable d'interrompre l'alimentation en cas d'urgence. Des sorties seront disponible entre les deux étages d'interrupteurs pour des systèmes qui ne sont pas dangereux et où des coupures abruptes de tension sont problématique. Notamment, pour la Pi. Finalement, le circuit aura un afficheur permettant l'indication de la tension de la batterie, ainsi que des DELs permettant l'indication de l'état du système.

\subsubsection{échéancier}

Les tâches à faire pour le système d'alimentation sont principalement la conception du circuit imprimé et la programmation de son microcontrôleur. La tâche de conception se divise dans la conception des schémas, la conception de la carte, la commande des circuits et des composantes et l'assemblage (soudage) des circuits. Ses tâches sont représentées dans la forme d'un diagramme de Gantt à la figure~\ref{fig:S2-6-échéance}.

Il est possible de voir que la conception des schémas ainsi que de la carte sont complétées. La tâche actuelle est l'attente des composantes. La tâche de programmation peut être commencée. Les fiches de données des composantes électriques peuvent être utilisées afin d'établir un code provisoire. De plus, la logique centrale peut déjà être définie. Cependant, sans la carte terminée, il est difficile de faire la programmation. Vu la simplicité du code, le fait que la moitié du temps alloué pour la tâche s'est écoulé ne devrait pas être un problème.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-échéance.png}
	\caption{Diagramme de Gantt représentant l'échéancier des tâches pour le système d'alimentation}
	\label{fig:S2-6-échéance}
\end{figure}

\subsubsection{Travail accompli}

\paragraph{Conception des schémas}

\subparagraph{Connexion de la batterie et protection}

La première feuille des schémas, présentée à la figure~\ref{fig:S2-6-sheet1}, fait la connexion de la batterie ainsi que la protection surtension, surcourant et polarité inverse. La protection surcourant est accompli en utilisant un fusible. Puisque la valeur du courant maximal du tableau~\ref{table:S2-6-courant-max} a déjà des facteurs de sécurité ajouté, il est possible de prendre un fusible pour sa pleine valeur. Ainsi, un fusible de $\SI{30}{\ampere}$ fut sélectionné.

La protection surtension est réussie avec une diode Zener de valeur légèrement supérieure à la tension maximale de la batterie. Ainsi, une diode de $\SI{17}{\volt}$ fut choisie. La principe de fonctionnement est la suivante: si une tension supérieure à $\SI{17}{\volt}$ est appliquée au circuit, tout le courant passera dans la diode dans le sens inverse et sautera le fusible. La diode fut soigneusement choisie afin de supporter plus de courant que le fusible.

La protection contre la polarisation inverse est implémentée avec une diode en série. Cette diode évitera qu'un courant passe dans le sens inverse. Une diode Schottky fut utilisée afin de minimiser la chute de tension. Encore une fois, la diode a été sélectionnée afin de supporter des courants plus élevés que le fusible.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet1.png}
	\caption{Schéma de la connexion de la batterie et des éléments de protection}
	\label{fig:S2-6-sheet1}
\end{figure}

\subparagraph{Régulateurs de tension}

La deuxième feuille des schémas, présentée à la figure~\ref{fig:S2-6-sheet2}, connecte les régulateurs de tension. En effet, elle raccorde le régulateur de $\SI{6}{\volt}$ et celui de $\SI{12}{\volt}$ à la tension de la batterie (après la protection). Puisque des régulateurs externes furent utilisés, l'implémentation est simple.

Le schéma implémente aussi un régulateur $\SI{3.3}{\volt}$. Cette tension est seulement utilisé à l'intérieur de la carte, d'où le choix d'un simple circuit intégré plutôt qu'une carte fille.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet2.png}
	\caption{Schéma des régulateurs de tension}
	\label{fig:S2-6-sheet2}
\end{figure}

\subparagraph{Microcontrôleur}

La prochaine feuille des schémas, présentée à la figure~\ref{fig:S2-6-sheet3}, implémente le microcontrôleur. Le MCU utilisé est le \textit{STM32F103RCT6}, un microcontrôleur de \textit{STMicroelectronics}. Ce contrôleur fut choisi pour son grand nombre d'entrées / sorties et de ports de communication. La vitesse de l'horloge n'est pas très important vu le programme simple, mais la carte aura plusieurs périphériques qui communiquent avec des protocoles différents. De plus, le MCU a besoin de plusieurs entrées analogiques afin de lire les divers tensions dans le circuit. Un survol des fonctionnalités du microcontrôleur est présenté au tableau~\ref{table:S2-6-STM32F103RCT6}.

\begin{table}
	\caption{Caractéristiques du microcontrôleur \textit{STM32F103RCT6} utilisé comme contrôleur pour la carte d'alimentation \cite{STM32F103RCT6}}
	\label{table:S2-6-STM32F103RCT6}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Caractéristique} & \textbf{Valeur} \\
		\midrule
		Processeur & 32-bit @ $\SI{72}{\mega\hertz}$ \\
		\midrule
		Mémoire & \makecell[l]{
			$\SI{256}{\kilo\byte}$ Flash \\
			$\SI{48}{\kilo\byte}$ SRAM \\
		} \\
		\midrule
		Interfaces de communication & \makecell[l]{
			\ItwoC (2) \\
			UART (5) \\
			SPI (3) \\
			CAN (1) \\
			USB 2.0 (1) \\
			SDIO (1) \\
		} \\
		\bottomrule
	\end{tabularx}
\end{table}

Le gros de la feuille est les connexions entre le MCU et les autres composantes. Puisque celles-ci se trouvent sur des feuilles différentes, des étiquettes sont utilisée pour faire la liaison. La feuille contient également un grand nombre de condensateurs céramiques raccordés entre $\SI{3.3}{\volt}$ et $\SI{0}{\volt}$. Ceux-ci sont requis afin d'assurer une tension stable sur chacune des entrées d'alimentation du microcontrôleur. La fiche de données du contrôleur fut utilisé comme référence pour les valeurs de ces condensateurs \cite{STM32F103RCT6}. Finalement, un connecteur \textit{JTAG} est inclus pour permettre la programmation du microcontrôleur.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet3.png}
	\caption{Implémentation du microcontrôleur principal de la carte l'alimentation: le \textit{STM32F103RCT6}}
	\label{fig:S2-6-sheet3}
\end{figure}

\subparagraph{Interrupteurs}

La quatrième feuille des schémas définie le fonctionnement des interrupteurs. Un aperçu de la feuille est donné à la figure~\ref{fig:S2-6-sheet4}. Tel que précisé à la section~\ref{S2-6-choix-stratégie}, six sorties sont requis. Pour chacun des trois niveaux de tension, il doit avoir une sortie avant et après l'arrêt d'urgence. Ainsi, six interrupteurs sont requis.

Les interrupteurs utilisés sont des MOSFET type P. Plus précisément, ils sont le SQJ951EP de \textit{Vishay Siliconix}. Ce transistor fut choisi pour ses caractéristiques de tension et de courant maximal. Il est possible de voir que les transistors peuvent supporter la tension maximal du robot, la tension de la batterie, ainsi que le courant maximal d'une sortie ($\SI{20}{\ampere}$) sans problème.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet4.png}
	\caption{Interrupteurs et lecture de courant}
	\label{fig:S2-6-sheet4}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques des MOSFET type P utilisés en tant qu'interrupteurs pour la carte d'alimentation \cite{SQJ951EP}}
	\label{table:S2-6-mosfet-propriétés}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		$V_{DS}$ & $\SI{-30}{\volt}$ \\
		$R_{DS_{on}}$ à $V_{GS} = \SI{-10}{\volt}$ & $\SI{0.017}{\volt}$ \\
		$I_D$ & $\SI{-30}{\ampere}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

La figure~\ref{fig:S2-6-sheet4} montre que les grilles du premier étage d'interrupteurs sont commandées via un BJT. Cela permet de fournir à la grille la tension de la batterie au lieu de la tension $\SI{3.3}{\volt}$ de la sortie du microcontrôleur. Alimenter la grille avec cette tension significativement plus élevée permet de réduire drastique ment la résistance interne du transistor et de réduire ses pertes.

La feuille de la figure~\ref{fig:S2-6-sheet4} implémente également des capteurs de courant. Ces capteurs servent à lire le courant sortant de chacune des sorties de tension différentes. Les capteurs utilisés sont les ACS711 de \textit{Allegro MicroSystems, LLC}. Lesdits capteurs utilisent l'effet Hall afin de détecter l'intensité du courant et émettent une tension proportionnelle au courant détecté. Quelques propriétés des capteurs sont données au tableau~\ref{table:S2-6-ACS711} et un aperçu de la forme des capteurs est donné à la figure~\ref{fig:S2-6-ACS711}.

\begin{table}[H]
	\caption{Caractéristiques de courant, de sensibilité et d'erreur sur les capteurs ACS711KEXLT-31AB \cite{ACS711}}
	\label{table:S2-6-ACS711}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Courant maximal & $\SI{31}{\ampere}$ \\
		Sensibilité & $\SI{45}{\milli\volt\per\ampere}$ \\
		Erreur & $\pm\SI{4}{\percent}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-6-ACS711.png}
	\caption{Capteur de courant à effet Hall ACS711KEXLT-31AB utilisé afin de détecter les intensités de courant dans la carte d'alimentation~\cite{ACS711}}
	\label{fig:S2-6-ACS711}
\end{figure}

\subparagraph{Afficheur pour la tension de la batterie}

La prochaine feuille des schémas incorpore l'afficheur DEL 7 segments à trois caractères responsable d'afficher la tension de la batterie. Présentée à la figure~\ref{fig:S2-6-sheet5}, la feuille définie les connexions entre l'afficheur, son pilote et le microcontrôleur.

L'afficheur consiste du LDT-N2804RI de \textit{Lumex Opto/Components Inc.} C'est un afficheur DEL à trois caractères avec cathode commune. Un aperçu de l'afficheur est donné à la figure~\ref{fig:S2-6-LDT-N2804RI}. Un survol de ses caractéristiques est présenté au tableau~\ref{table:S2-6-LDT-N2804RI}. Ces valeurs seront utilisées afin de configurer le courant du pilote.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet5.png}
	\caption{Afficheur 7 segments affichant la tension de la batterie}
	\label{fig:S2-6-sheet5}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-6-LDT-N2804RI.png}
	\caption{Afficheur DEL 7 segments à trois caractères LDT-N2804RI~\cite{LDT-N2804RI}}
	\label{fig:S2-6-LDT-N2804RI}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques de l'afficheur DEL 7 segments LDT-N2804RI~\cite{LDT-N2804RI}}
	\label{table:S2-6-LDT-N2804RI}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Chute de tension directe typique & $\SI{2}{\volt}$ \\
		Courant direct (DEL) & $\SI{10}{\milli\ampere}$ \\
		Consommation & $\SI{105}{\milli\watt}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Afin de simplifier le contrôle de l'afficheur, un pilote est utilisé. Le pilote s'agit du AS1108WL-T de \textit{austriamicrosystems AG}. Tel que montre la figure~\ref{fig:S2-6-sheet5}, le pilote se connecte aux 8 segments (7 plus point) de l'afficheur et aux trois entrées qui sélectionnent le chiffre. L'appareil se connecte également au microcontrôleur avec le protocole SPI.

La résistance R6 qui se trouve entre les pins VDD et ISET est responsable de la configuration de l'intensité de courant que le pilote insèrera dans les segments de l'afficheur. Sa valeur fut déterminée en fonction des données du tableau~\ref{table:S2-6-LDT-N2804RI} et à l'aide du tableau~20 de la fiche de donnée du pilote~\cite{AS1108WL-T}.

\subparagraph{Carte SD}

La feuille présentée à la figure~\ref{fig:S2-6-sheet6} connecte la carte SD au microcontrôleur. Le connecteur en question et le 0472192001 de \textit{Molex}. Il est connecté au microcontrôleur avec le protocole SDIO. Ce protocole est électriquement simple: il suffit de raccorder les fils SDIO\_CMD, SDIO\_CLK et SDIO\_D0 à SDIO\_D3 aux pins correspondantes du microcontrôleur avec des résistances d'excursion haute. Un aperçu du connecteur pour la carte SD est donné à la figure~\ref{fig:S2-6-connecteur-sd}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet6.png}
	\caption{Connexion de l'adaptateur pour la carte SD}
	\label{fig:S2-6-sheet6}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-6-0472192001.png}
	\caption{Connecteur SD \textit{Molex}~\cite{0472192001}}
	\label{fig:S2-6-connecteur-sd}
\end{figure}

\subparagraph{Diviseurs de tension}

Puisque le STM32F103RCT6 est un microcontrôleur de $\SI{3.3}{\volt}$, il est nécessaire de réduire les tensions aux entrées analogiques à l'intervalle de $\SI{0}{\volt}$ à $\SI{3.3}{\volt}$. Les diviseurs de tensions présentés à la figure~\ref{fig:S2-6-sheet7} permettent de réduire les tensions de la batterie et des régulateurs à des tensions lisibles par le MCU. Deux diviseurs sont utilisés pour les deux régulateurs. Quatre autres sont utilisés pour lire les tensions des quatre cellules de la batterie. Les résistances raccordées entre l'entrée et la terre furent fixées à $\SI{100}{\kilo\ohm}$ et les résistances entre la tension à lire et l'entrée furent calculées à l'aide d'un tableur.

La feuille inclut également le connecteur pour la lecture des tensions de la batterie. Le connecteur s'agit du B5B-PH-SM4 de \textit{JST}, conformément à la batterie. Puisque les cellules sont branchés en série, la tension aux bornes d'un cellule peut être calculée par la différence de deux tensions séries. Un aperçu du connecteur B5B-PH-SM4 est donné à la figure~\ref{fig:S2-6-JST}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet7.png}
	\caption{Diviseurs de tension}
	\label{fig:S2-6-sheet7}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-6-JST.png}
	\caption{Connecteur \textit{JST} B5B-PH-SM4 utilisé pour la lecture des tensions des cellules de la batterie~\cite{JST}}
	\label{fig:S2-6-JST}
\end{figure}

\subparagraph{Sorties de la carte}

La huitième feuille des schémas, présentée à la figure~\ref{fig:S2-6-sheet8}, contient les sorties de la carte. Trois connecteurs sont inclus pour les trois tensions de sorties. Les connecteurs sont de la série COMBICON SPT de \textit{Phoenix Contact}. Ils furent choisies puisqu'ils était utilisés l'année passée et cela permet de les réutiliser. Un connecteur $2 \times 6$ est utilisé pour les sorties à la tension de la batterie, les sorties $\SI{12}{\volt}$ utilise le connecteur $2 \times 10$ et un connecteur $2 \times 12$ est utilisé pour $\SI{6}{\volt}$. Ces connecteurs sont visibles à la figure~\ref{fig:S2-6-conneecteurs-phoenix-contact}.

\begin{figure}[H]
  \centering
  \begin{subfigure}{0.3\textwidth}
    \includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-PC-6.jpg}
    \caption{Connecteur $2 \times 6$~\cite{PC-6}}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.3\textwidth}
    \includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-PC-10.jpg}
    \caption{Connecteur $2 \times 10$~\cite{PC-10}}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.3\textwidth}
    \includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-PC-12.jpg}
    \caption{Connecteur $2 \times 12$~\cite{PC-12}}
  \end{subfigure}
  \caption{Connecteurs COMBICON SPT de \textit{Phoenix Contact}}
  \label{fig:S2-6-conneecteurs-phoenix-contact}
\end{figure}

La feuille de la figure~\ref{fig:S2-6-sheet8} inclut également la connexion de l'arrêt d'urgence. Lorsque le bouton est ouvert, la grille du transistor est tirée à la tension de la batterie par une résistance d'excursion haute. Lorsque le bouton est fermé, la grille est tirée à $\SI{0}{\volt}$. Le connecteur utilisé est de la série COMBICON TDPT de \textit{Phoenix Contact}. Un aperçu du connecteur est présenté à la figure~\ref{fig:S2-6-PC2}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-6-PC-2.jpg}
	\caption{Connecteur COMBICON TDPT de \textit{Phoenix Contact}}
	\label{fig:S2-6-PC2}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet8.png}
	\caption{Sorties de la carte d'alimentation}
	\label{fig:S2-6-sheet8}
\end{figure}

\subparagraph{Connexion UART}

La figure~\ref{fig:S2-6-sheet9} montre la neuvième feuille des schémas de la carte d'alimentation. Elle inclut une seule composante: un connecteur pour un convertisseur USB à UART tel que celui présenté à la figure~\ref{fig:S2-6-FTDI}. La connexion UART au microcontrôleur sera utilisé pour dépanner le micrologiciel. Il fut décidé d'utiliser un convertisseur externe pour simplifier le circuit et puisque le GRUM possède déjà des convertisseurs compatibles.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet9.png}
	\caption{Connecteur pour le convertisseur USB à UART}
	\label{fig:S2-6-sheet9}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-6-FTDI.jpg}
	\caption{Convertisseur USB à UART compatible avec le connecteur de la figure~\ref{fig:S2-6-sheet9}~\cite{FTDI}}
	\label{fig:S2-6-FTDI}
\end{figure}

\subparagraph{Indicateurs d'état}

La dixième et finale feuilles des schémas incorpore les indicateurs d'état. Elle est présentée à la figure~\ref{fig:S2-6-sheet10}. L'indication de l'état de l'arrêt d'urgence est accompli par une DEL. Un comparateur est utilisé pour injecter un courant dans la DEL si la tension aux bornes de l'arrêt d'urgence surpasse $\SI{3.3}{\volt}$. Cette condition est atteinte si le bouton est ouvert.

Deux DELs antiparallèle rouges / vertes sont utilisés sont utilisées pour l'indication de l'état de la batterie et des régulateurs. Les DELs sont connectées entre deux sorties du microcontrôleur de telle façon que celui ci peut injecter un courant dans les deux sensés, alternant la couleur de la DEL.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-schéma-sheet10.png}
	\caption{Indicateurs de l'état du système d'alimentation}
	\label{fig:S2-6-sheet10}
\end{figure}

\paragraph{Conception de la carte}

Avec les schémas du circuit d'alimentations complets, il fut possible de concevoir la carte. Prime d'abord, les composantes furent positionnées à des endroits logiques qui facilitent les connexions ainsi que les branchements à la carte assemblée.

Les positions des composantes principales sont montrées à la figure~\ref{fig:S2-6-carte-composantes}. Le tableau~\ref{table:S2-6-carte-composantes-légende} s'agit de la Legendre de cette figure.

\clearpage
\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-carte-composantes.png}
	\caption{Locations des composantes sur la carte d'alimentation}
	\label{fig:S2-6-carte-composantes}
\end{figure}

\begin{table}[H]
	\caption{Légende des composantes de la figure~\ref{fig:S2-6-carte-composantes}}
	\label{table:S2-6-carte-composantes-légende}
	\begin{tabularx}{\textwidth}{rX}
		\toprule
		\textbf{Numéro} & \textbf{Nom de la composante} \\
		\midrule
		1 & Connexion à la batterie \\
		2 & Régulateur de tension $\SI{3.3}{\volt}$ \\
		3 & Régulateur de tension $\SI{6}{\volt}$ \\
		4 & Régulateur de tension $\SI{12}{\volt}$ \\
		5 & Microcontrôleur \\
		6 & Connexion JTAG \\
		7 & Afficheur 7 segments \\
		8 & Pilote de l'afficheur 7 segments \\
		9 & Carte SD \\
		10 & Connexion de l'arrêt d'urgence \\
		11 & Sorties à la tension de la batterie \\
		12 & Sorties $\SI{12}{\volt}$ \\
		13 & Sorties $\SI{6}{\volt}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Une fois que les composantes furent positionnées, les connexions électriques entre les éléments ont été crée. La figure~\ref{fig:S2-6-cartes-routées} montre la carte avec ces connexions. Vu les hautes intensités de courant dans cette carte, la largeur des connexions a dû être choisie soigneusement. En effet, les liaisons entre la batterie, les régulateurs et les sorties sont significativement plus larges que les signaux. De plus, l'outil \textquote{polygon} fut exploité afin de maximiser la largeur des fils dans des régions coincés. Un exemple de cela est donné à la figure~\ref{fig:S2-6-carte-poly}.

\begin{figure}[H]
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-carte-both.png}
		\caption{Couche supérieure et couche inférieure}
		\label{fig:S2-6-cartes-routées-both}
	\end{subfigure}
	\caption{Carte d'alimentation avec les connexions électriques}
\end{figure}

\begin{figure}[H]\ContinuedFloat
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-carte-top.png}
		\caption{Couche supérieure}
		\label{fig:S2-6-cartes-routées-top}
	\end{subfigure}
	\\
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-6-carte-bottom.png}
		\caption{Couche inférieure}
		\label{fig:S2-6-cartes-routées-bottom}
	\end{subfigure}
	\caption{Carte d'alimentation avec les connexions électriques (cont.)}
	\label{fig:S2-6-cartes-routées}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-carte-poly.png}
	\caption{Utilisation de l'outil \textquote{polygon} d'\textit{Eagle} pour maximiser la largeur des connexions}
	\label{fig:S2-6-carte-poly}
\end{figure}
